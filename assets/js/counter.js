/*
	Fractal by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {
	$.fn.digits = function(){ 
		return this.each(function(){ 
			$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
		})
	}

	var thisDate = new Date();
	var thisMonth = thisDate.getMonth();

	for (i = 0; i < thisMonth; i++) {
		$('#month-' + i).removeClass('fa-circle-o').addClass('fa-circle')
	}

	$('#month-' + thisMonth).removeClass('fa-circle-o').addClass('fa-check-circle')

	$.get("/data.json", function(data) {
		var now = new Date();
		let last30 = 0;

		data.sort((a,b) => (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0)); 

		for (i = 0; i < 31; i++) {
			last30 += data[i].count;
		}

		let avg = last30 / 30
		let ytarget = avg * 365
		console.log(ytarget);

		let startofyear = new Date(now.getFullYear(), 0, 1);
		console.log(startofyear);

		var bpm = (ytarget / 365) /  86400000 // Builds per year / days per year / milliseconds per day

		let total = Math.round((now - startofyear) * bpm);

		$('.counter').text(total).digits();

		setInterval(function() {
			total += Math.round((new Date() - now) * bpm)
			$('.counter').text(total).digits();
			now = new Date();
		}, 1000)
	})

	/*
	$('.counter').each(function() {
		var $this = $(this),
			countTo = $this.attr('data-count');
		
		$({ countNum: $this.text()}).animate({
		  countNum: countTo
		},
	  
		{
	  
		  duration: 8000,
		  easing:'linear',
		  step: function() {
			$this.text(Math.floor(this.countNum)).digits();
		  },
		  complete: function() {
			$this.text(this.countNum).digits();
			//alert('finished');
		  }
	  
		});  
		
		
	  
	  });
	  */

})(jQuery);